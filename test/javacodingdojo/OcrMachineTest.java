package javacodingdojo;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OcrMachineTest {

    @org.junit.Test
    public void getFirstDigitAsString() throws Exception {
        String testLine = " _  _  _  _  _  _  _  _  _ \n" +
                          "| || || || || || || || || |\n" +
                          "|_||_||_||_||_||_||_||_||_|\n" +
                          "                           \n";

        OcrMachine ocrMachine = new OcrMachine();

        String expected = " _ " +
                          "| |" +
                          "|_|";

        assertEquals(expected, ocrMachine.getFirstDigitAsString(testLine));
    }

    @Test
    public void testGetSecondDigitAsString() throws Exception {
        String testLine = "    _  _     _  _  _  _  _ \n" +
                          "  | _| _||_||_ |_   ||_||_|\n" +
                          "  ||_  _|  | _||_|  ||_| _|\n" +
                          "                           \n";

        OcrMachine ocrMachine = new OcrMachine();

        String expected = " _ " +
                " _|" +
                "|_ ";

        int count = 2;

        assertEquals(expected, ocrMachine.getNthDigitAsString(count, testLine));
    }

    @Test
    public void testMapStringForOneToInteger() {
        OcrMachine ocrMachine = new OcrMachine();
        String one = "   "+"  |"+"  |";
        assertEquals(1, ocrMachine.mapStringToDigit(one));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testNegaticNthDigit() {
        OcrMachine ocrMachine = new OcrMachine();
        ocrMachine.getNthDigitAsString(-1, "");
    }

    @Test
    public void mapStringToIntegerTest(){

    }



    }
