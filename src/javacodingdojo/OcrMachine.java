package javacodingdojo;

import java.util.HashMap;
import java.util.Map;

public class OcrMachine {
    public String getFirstDigitAsString(String strInput) {

        return strInput.substring(0, 3) +
                strInput.substring(28, 31) +
                strInput.substring(56, 59);

    }

    public String getNthDigitAsString(int count, String testLine) {
        if(count < 0)
            throw new IllegalArgumentException();

        return testLine.substring(0 + (count -1) * 3, 3 + (count -1)* 3) +
                testLine.substring(28 + (count -1)  * 3, 31 + (count -1) * 3) +
                testLine.substring(56 + (count -1) * 3, 59 + (count -1) * 3);

    }


    public int mapStringToDigit(String digitalInputNumber) {

        Map<String, Integer> digitMap = new HashMap<String, Integer>();

        String digitOne = "   "
                   + "  |"
                   + "  |";

        digitMap.put(digitOne, 1);


        int ret = digitMap.get(digitalInputNumber);

        return ret;
    }



}
